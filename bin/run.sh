#!/bin/sh

docker run --interactive --tty --rm --volume $(pwd):/root/ens --name ens-dns ens-dns:latest
