# Dockerfile

FROM debian:bullseye

RUN apt-get update
RUN apt-get install --yes curl dnsutils git gnupg

# NodeJS is too old on Debian Bullseye
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install --yes nodejs

# `WORKDIR` must come before `npm install`
WORKDIR /root

RUN npm install colors content-hash ethers

# DNSdist and PowerDNS Authoritative Server are too old on Debian Bullseye
RUN echo "Package: dnsdist*\nPin: origin repo.powerdns.com\nPin-Priority: 600" > /etc/apt/preferences.d/pdns
RUN echo "deb [arch=amd64] http://repo.powerdns.com/debian bullseye-dnsdist-18 main" > /etc/apt/sources.list.d/pdns.list
RUN echo "deb [arch=amd64] http://repo.powerdns.com/debian bullseye-auth-47 main" >> /etc/apt/sources.list.d/pdns.list
RUN curl https://repo.powerdns.com/CBC8B383-pub.asc -o /etc/apt/trusted.gpg.d/CBC8B383-pub.asc
RUN curl https://repo.powerdns.com/FD380FBB-pub.asc -o /etc/apt/trusted.gpg.d/FD380FBB-pub.asc
RUN apt-get update
RUN apt-get install --yes dnsdist pdns-server pdns-backend-remote

RUN echo "alias l='ls -l'" >> .bashrc
