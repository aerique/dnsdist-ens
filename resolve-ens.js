// resolve-ens.js

// Packages

const http = require('http')
const os   = require('os')
const url  = require('url')

const colors      = require('colors')
const contentHash = require('content-hash')
const ethers      = require('ethers')


// Globals

const host = 'localhost'
const port = 3500


// Functions

function ethA (qname, content)
{
    return { 'qtype': 'A',
             'qname': qname,
             'content': content,
             'ttl': 60 }
}


function ethCNAME (qname, content)
{
    return { 'qtype': 'CNAME',
             'qname': qname,
             'content': content,
             'ttl': 60 }
}


function ethSOA (qname)
{
    return { 'qtype': 'SOA',
             'qname': qname,
             'content': 'ns.eth. hostmaster.eth. 2023041700 ' +
                        '7200 3600 1209600 3600',
             'ttl': 3600 }
}


function respondEmpty (response)
{
    console.log('  • returning empty set')
    response.writeHead(200)
    response.end(JSON.stringify({ 'result': [] }))
}


function respondFallback (response, qname, ens_name)
{
    console.log(`  • returning "${ens_name}.limo." as fallback`)
    //var eth_link = ens_name + '.link.'
    // `eth.link` is broken at the moment
    var eth_link = ens_name + '.limo.'
    response.writeHead(200)
    response.end(JSON.stringify({ 'result': [ ethCNAME(qname, eth_link) ] }))
}


// Handlers

function handleGetAllDomains (response)
{
    response.writeHead(200)
    response.end(JSON.stringify({ 'result': [{ 'zone': 'eth.' }] }))
}


function handleLookup (response, method)
{
    var qitems = method.split('/')
    console.log('  • [' + qitems + ']')
    var qname = qitems[1]
    var qtype = qitems[2]
    if (qtype == 'ANY' && qname == 'eth.') {
        console.log('  • returning SOA')
        response.writeHead(200)
        response.end(JSON.stringify({ 'result': [ ethSOA(qname) ]}))
    } else if (qtype == 'ANY' && qname == '*.eth.') {
        respondEmpty(response)
    } else if (qtype == 'ANY' && qname.length > 5) {
        var ens_name = qname.substring(0, qname.length - 1)
        provider.getResolver(ens_name).then((resolver) => {
            //console.log(`  • "${ens_name}" resolver is ${resolver.address}`)
            resolver.getContentHash().then((ens_ch) => {
                if (ens_ch != null && ens_ch.substring(0,7) == 'ipfs://') {
                    console.log(`  • "${ens_name}" has an IPFS content hash ` +
                                `${ens_ch}`)
                    var ch = contentHash.helpers.cidV0ToV1Base32(
                        ens_ch.substring(7))
                    var ipfs_link = ch + '.ipfs.dweb.link.'
                    console.log(`  • returning "${ipfs_link}"`)
                    response.writeHead(200)
                    response.end(JSON.stringify(
                                { 'result': [ ethCNAME(qname, ipfs_link) ]}))
                } else {
                    console.log(`  • "${ens_name}" does not have an IPFS content hash`.yellow)
                    resolver.getText('cname').then((ens_cname) => {
                        if (ens_cname != '') {
                            console.log(`  • "${ens_name}" has CNAME ` +
                                        `${ens_cname}`)
                            console.log(`  • returning "${ens_cname}"`)
                            response.writeHead(200)
                            response.end(JSON.stringify(
                                { 'result': [ ethCNAME(qname, ens_cname) ]}))
                        } else {
                            console.log(`  • "${ens_name}" does not have CNAME set`.yellow)
                            respondFallback(response, qname, ens_name)
                        }
                    }).catch(() => {
                        console.log(`  • could get neither content hash nor ` +
                                    `"cname" for "${ens_name}"`.red)
                        respondEmpty(response)
                    })
                }
            }).catch(() => {
                console.log('  • error retrieving content hash for ' +
                            `"${ens_name}"`.red)
                respondEmpty(response)
            })
        }).catch(() => {
            console.log(`  • could not get resolver for "${ens_name}"`.red)
            respondEmpty(response)
        })
    } else {
        console.log(`  • don't know what to do with ${qname}/${qtype}`.yellow)
        respondEmpty(response)
    }
}


// Main

// Doesn't work at the moment.
//const provider = ethers.getDefaultProvider()
const provider = new ethers.JsonRpcProvider(
                         'https://eth-rpc.gateway.pokt.network/')

provider.getBlockNumber().then((bn) => {
    console.log('Connected to Ethereum, we are at block ' + bn)
}).catch(() => {
    console.log('Failed getting Ethereum block number'.red)
    // XXX Should we just exit here?
})

// Sanity check
provider.resolveName('eth').then((addr) => {
    console.log('"eth" resolves to ' + addr)
}).catch(() => {
    console.log('Failed resolving "eth"'.red)
})

const server = http.createServer(function(request, response) {
    console.log(`• [${request.method}] ${decodeURI(request.url)}`.green)
    var parsed_url = url.parse(decodeURI(request.url))
    //console.dir(parsed_url)
    //console.log(`  • pathname: ${parsed_url.pathname}\n` +
    //            `  • query: ${parsed_url.query}`)
    if (parsed_url.pathname.startsWith('/ens/')) {
        var method = parsed_url.pathname.substring(5)  // `/ens/` is length 5
        //console.log(`  • method: ${method}`)
        if (method.startsWith('getAllDomains')) {
            handleGetAllDomains(response)
        } else if (method.startsWith('getAllDomainMetadata')) {
            respondEmpty(response)
        } else if (method.startsWith('lookup')) {
            handleLookup(response, method)
        } else {
            console.log(`  • unsupported method "${method}"`.red)
            // `{result:false}` should only be used on network errors
            //response.writeHead(200)
            //response.end(JSON.stringify({ 'result': false }))
            respondEmpty(response)
        }
    } else {
        console.log('  • not an ENS query, sending "fail".'.red)
        // Not sure what to respond in this case. "Shouldn't happen."
        response.writeHead(200)
        response.end(JSON.stringify({ 'result': false }))
    }
})

server.listen(port, host)
console.log(`ENS backend for pdns_server listening at http://${host}:${port}`)
